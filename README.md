# ctftp -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/ctftp)

## Chal Info

Desc: `Just what the world needs... another vulnerable FTP server. Have fun.`

Flag: `TUCTF{f1l73r_f1r57_7h3y_541d._y0u'll_b3_53cur3_7h3y_541d}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/ctftp)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/ctftp:tuctf2019
```
